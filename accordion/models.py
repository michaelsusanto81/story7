from django.db import models

# Create your models here.
class Activity(models.Model):
	act_title = models.CharField(max_length=50)
	act_desc = models.CharField(max_length=200)

class Experience(models.Model):
	exp_title = models.CharField(max_length=50)
	exp_desc = models.CharField(max_length=200)

class Achievement(models.Model):
	achv_title = models.CharField(max_length=50)
	achv_desc = models.CharField(max_length=200)