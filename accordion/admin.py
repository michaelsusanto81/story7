from django.contrib import admin
from accordion.models import Activity, Experience, Achievement

# Register your models here.
admin.site.register(Activity)
admin.site.register(Experience)
admin.site.register(Achievement)