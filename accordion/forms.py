from django import forms
from accordion.models import Activity, Experience, Achievement

class ActivityForm(forms.ModelForm):
	act_title = forms.CharField(label='Activity Title ', widget=forms.TextInput(attrs={'class': 'textinput'}))
	act_desc = forms.CharField(label='Activity Description ', widget=forms.Textarea(attrs={'class': 'textarea'}))
	field_order = ['act_title', 'act_desc']

	class Meta:
		model = Activity
		fields = {'act_title', 'act_desc'}

class ExperienceForm(forms.ModelForm):
	exp_title = forms.CharField(label='Experience Title ', widget=forms.TextInput(attrs={'class': 'textinput'}))
	exp_desc = forms.CharField(label='Experience Description ', widget=forms.Textarea(attrs={'class': 'textarea'}))
	field_order = ['exp_title', 'exp_desc']

	class Meta:
		model = Experience
		fields = {'exp_title', 'exp_desc'}

class AchievementForm(forms.ModelForm):
	achv_title = forms.CharField(label='Achievement Title ', widget=forms.TextInput(attrs={'class': 'textinput'}))
	achv_desc = forms.CharField(label='Achievement Description ', widget=forms.Textarea(attrs={'class': 'textarea'}))
	field_order = ['achv_title', 'achv_desc']

	class Meta:
		model = Achievement
		fields = {'achv_title', 'achv_desc'}