from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.http import HttpRequest
from accordion.views import *
from accordion.models import Activity, Experience, Achievement
from accordion.forms import ActivityForm, ExperienceForm, AchievementForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class AccordionUnitTest(TestCase):

	@classmethod
	def setUpTestData(cls):
		Activity.objects.create(act_title='Working', act_desc='Working on story 7 PPW.')
		Experience.objects.create(exp_title='Project: Rock Paper Scissors', exp_desc='A rock-paper-scissors game made by myself.')
		Achievement.objects.create(achv_title='SEA Compfest 2018 Best Team', achv_desc='Best team that worked on simple wallet mobile\'s App by COMPFEST X GOJEK.')

	# test if url is exist
	def test_home_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	# test if url is not exist
	def test_url_is_not_exist(self):
		response = Client().get('/wek/')
		self.assertEqual(response.status_code, 404)

	# test if home using index func
	def test_home_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	# test if home using index.html
	def test_home_using_index_template(self):
		response = Client().get('/')
		self.assertTemplateUsed(response, 'index.html')

	# test if home landing page is completed
	def test_home_landing_page_is_completed(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Michael\'s Portfolio', html_response)

	# test if Activity model is in database
	def test_activity_is_in_database(self):
		act = Activity.objects.create(act_title="ActTest2", act_desc="ActTest2Desc")
		total_act = Activity.objects.all().count()
		self.assertEqual(total_act, 2)
	
	# test Activity model fields are exist
	def test_activity_fields_are_exist(self):
		actObj = Activity.objects.get(id=1)
		title = Activity._meta.get_field('act_title').verbose_name
		self.assertEqual(title, 'act title')
		desc = Activity._meta.get_field('act_desc').verbose_name
		self.assertEqual(desc, 'act desc')

	# test Activity model fields max_length
	def test_activity_fields_max_length(self):
		actObj = Activity.objects.get(id=1)
		title_length = Activity._meta.get_field('act_title').max_length
		self.assertEqual(title_length, 50)
		desc_length = Activity._meta.get_field('act_desc').max_length
		self.assertEqual(desc_length, 200)

	# test if Experience model is in database
	def test_experience_is_in_database(self):
		exp = Experience.objects.create(exp_title="ExpTest2", exp_desc="ExpTest2Desc")
		total_exp = Experience.objects.all().count()
		self.assertEqual(total_exp, 2)

	# test Experience model fields are exist
	def test_experience_fields_are_exist(self):
		expObj = Experience.objects.get(id=1)
		title = Experience._meta.get_field('exp_title').verbose_name
		self.assertEqual(title, 'exp title')
		desc = Experience._meta.get_field('exp_desc').verbose_name
		self.assertEqual(desc, 'exp desc')

	# test Experience model fields max_length
	def test_experience_fields_max_length(self):
		expObj = Experience.objects.get(id=1)
		title_length = Experience._meta.get_field('exp_title').max_length
		self.assertEqual(title_length, 50)
		desc_length = Experience._meta.get_field('exp_desc').max_length
		self.assertEqual(desc_length, 200)

	# test if Achievement model is in database
	def test_achievement_is_in_database(self):
		achv = Achievement.objects.create(achv_title="AchvTest2", achv_desc="AchvTest2Desc")
		total_achv = Achievement.objects.all().count()
		self.assertEqual(total_achv, 2)

	# test Achievement model fields are exist
	def test_achievement_fields_are_exist(self):
		achvObj = Achievement.objects.get(id=1)
		title = Achievement._meta.get_field('achv_title').verbose_name
		self.assertEqual(title, 'achv title')
		desc = Achievement._meta.get_field('achv_desc').verbose_name
		self.assertEqual(desc, 'achv desc')

	# test Achievement model fields max_length
	def test_achievement_fields_max_length(self):
		achvObj = Achievement.objects.get(id=1)
		title_length = Achievement._meta.get_field('achv_title').max_length
		self.assertEqual(title_length, 50)
		desc_length = Achievement._meta.get_field('achv_desc').max_length
		self.assertEqual(desc_length, 200)

	# test if activity form has placeholder
	def test_activity_form_has_placeholder(self):
		form = ActivityForm()
		self.assertIn('id="id_act_title', form.as_p())
		self.assertIn('id="id_act_desc', form.as_p())

	# test activity form blank validation
	def test_activity_form_validation_for_blank_items(self):
		form = ActivityForm(data={'act_title':'', 'act_desc':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['act_title'], ["This field is required."])
		self.assertEqual(form.errors['act_desc'], ["This field is required."])

	# test if experience form has placeholder
	def test_experience_form_has_placeholder(self):
		form = ExperienceForm()
		self.assertIn('id="id_exp_title', form.as_p())
		self.assertIn('id="id_exp_desc', form.as_p())

	# test experience form blank validation
	def test_experience_form_validation_for_blank_items(self):
		form = ExperienceForm(data={'exp_title':'', 'exp_desc':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['exp_title'], ["This field is required."])
		self.assertEqual(form.errors['exp_desc'], ["This field is required."])

	# test if achievement form has placeholder
	def test_achievement_form_has_placeholder(self):
		form = AchievementForm()
		self.assertIn('id="id_achv_title', form.as_p())
		self.assertIn('id="id_achv_desc', form.as_p())

	# test achievement form blank validation
	def test_achievement_form_validation_for_blank_items(self):
		form = AchievementForm(data={'achv_title':'', 'achv_desc':''})
		self.assertFalse(form.is_valid())
		self.assertEqual(form.errors['achv_title'], ["This field is required."])
		self.assertEqual(form.errors['achv_desc'], ["This field is required."])

	# test activity form url is exist
	def test_if_add_activity_form_is_exist(self):
		response = Client().get('/addActivity/')
		self.assertEqual(response.status_code, 200)

	# test activity landing page
	def test_activity_landing_page(self):
		request = HttpRequest()
		response = addActivity(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Add Activity', html_response)

	# test if activity using addActivity func
	def test_activity_using_addActivity_func(self):
		found = resolve('/addActivity/')
		self.assertEqual(found.func, addActivity)

	# test if activity using activity.html
	def test_activity_using_activity_template(self):
		response = Client().get('/addActivity/')
		self.assertTemplateUsed(response, 'activity.html')

	# test submit activity
	def test_add_activity_success(self):
		test = 'Test'
		response_post = Client().post('/addActivity/', {'act_title':test, 'act_desc':test})
		self.assertEqual(response_post.status_code, 302)

	# test remove activity
	def test_remove_activity(self):
		request = HttpRequest()
		response = removeActivity(request, 1)
		self.assertEqual(Activity.objects.all().count(), 0)

	# test experience form url is exist
	def test_if_add_experience_form_is_exist(self):
		response = Client().get('/addExperience/')
		self.assertEqual(response.status_code, 200)

	# test experience landing page
	def test_experience_landing_page(self):
		request = HttpRequest()
		response = addExperience(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Add Experience', html_response)

	# test if experience using addExperience func
	def test_experience_using_addExperience_func(self):
		found = resolve('/addExperience/')
		self.assertEqual(found.func, addExperience)

	# test if experience using experience.html
	def test_experience_using_experience_template(self):
		response = Client().get('/addExperience/')
		self.assertTemplateUsed(response, 'experience.html')

	# test submit experience
	def test_add_experience_success(self):
		test = 'Test'
		response_post = Client().post('/addExperience/', {'exp_title':test, 'exp_desc':test})
		self.assertEqual(response_post.status_code, 302)

	# test remove experience
	def test_remove_experience(self):
		request = HttpRequest()
		response = removeExperience(request, 1)
		self.assertEqual(Experience.objects.all().count(), 0)

	# test achievement form url is exist
	def test_if_add_achievement_form_is_exist(self):
		response = Client().get('/addAchievement/')
		self.assertEqual(response.status_code, 200)

	# test achievement landing page
	def test_achievement_landing_page(self):
		request = HttpRequest()
		response = addAchievement(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Add Achievement', html_response)

	# test if achievement using addAchievement func
	def test_achievement_using_addAchievement_func(self):
		found = resolve('/addAchievement/')
		self.assertEqual(found.func, addAchievement)

	# test if achievement using achievement.html
	def test_achievement_using_achievement_template(self):
		response = Client().get('/addAchievement/')
		self.assertTemplateUsed(response, 'achievement.html')

	# test submit achievement
	def test_add_achievement_success(self):
		test = 'Test'
		response_post = Client().post('/addAchievement/', {'achv_title':test, 'achv_desc':test})
		self.assertEqual(response_post.status_code, 302)

	# test remove achievement
	def test_remove_achievement(self):
		request = HttpRequest()
		response = removeAchievement(request, 1)
		self.assertEqual(Achievement.objects.all().count(), 0)

class AccordionFunctionalTest(LiveServerTestCase):

	# set up selenium
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--window-size=1420,1080')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)

    # close selenium
    def tearDown(self):
        self.selenium.quit()
        super(AccordionFunctionalTest, self).tearDown()

    # main flow of functional test
    def test_input_todo(self):

    	# open selenium
        selenium = self.selenium

        # open localhost
        selenium.get('http://127.0.0.1:8000/')

        # find buttons
        add_activity_button = selenium.find_element_by_id('addActivity')
        add_experience_button = selenium.find_element_by_id('addExperience')
        add_achievement_button = selenium.find_element_by_id('addAchievement')

        # Activity Flow
        add_activity_button.send_keys(Keys.RETURN)
        # fill activity form with data
        act_title = selenium.find_element_by_id('id_act_title')
        act_desc = selenium.find_element_by_id('id_act_desc')
        act_submit = selenium.find_element_by_tag_name('button')
        act_title.send_keys('Test Activity')
        act_desc.send_keys('Test Activity Description')
        act_submit.send_keys(Keys.RETURN)
        self.assertIn('Test Activity', self.selenium.page_source)
        self.assertIn('Test Activity Description', self.selenium.page_source)