from django.urls import path
from . import views

app_name = "accordion"

urlpatterns = [
    path('', views.index, name='index'),
    path('addActivity/', views.addActivity, name='addActivity'),
    path('addExperience/', views.addExperience, name='addExperience'),
    path('addAchievement/', views.addAchievement, name='addAchievement'),
    path('removeActivity/<int:act_id>', views.removeActivity, name='removeActivity'),
    path('removeExperience/<int:exp_id>', views.removeExperience, name='removeExperience'),
    path('removeAchievement/<int:achv_id>', views.removeAchievement, name='removeAchievement'),
]