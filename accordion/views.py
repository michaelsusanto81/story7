from django.shortcuts import render, redirect
from accordion.models import Activity, Experience, Achievement
from accordion.forms import ActivityForm, ExperienceForm, AchievementForm

# Create your views here.
def index(request):
	response = {}
	response['activity'] = Activity.objects.all()
	response['experience'] = Experience.objects.all()
	response['achievement'] = Achievement.objects.all()
	return render(request, 'index.html', response)

def addActivity(request):
	response = {}
	actForm = ActivityForm(request.POST or None)
	if(request.method == 'POST' and actForm.is_valid()):
		activity = Activity(act_title=request.POST['act_title'], act_desc=request.POST['act_desc'])
		activity.save()
		return redirect('/')
	response['actForm'] = actForm
	return render(request, 'activity.html', response)

def addExperience(request):
	response = {}
	expForm = ExperienceForm(request.POST or None)
	if(request.method == 'POST' and expForm.is_valid()):
		experience = Experience(exp_title=request.POST['exp_title'], exp_desc=request.POST['exp_desc'])
		experience.save()
		return redirect('/')
	response['expForm'] = expForm
	return render(request, 'experience.html', response)

def addAchievement(request):
	response = {}
	achvForm = AchievementForm(request.POST or None)
	if(request.method == 'POST' and achvForm.is_valid()):
		achievement = Achievement(achv_title=request.POST['achv_title'], achv_desc=request.POST['achv_desc'])
		achievement.save()
		return redirect('/')
	response['achvForm'] = achvForm
	return render(request, 'achievement.html', response)

def removeActivity(request, act_id):
	activity = Activity.objects.get(id=act_id)
	activity.delete()
	return redirect('/')

def removeExperience(request, exp_id):
	experience = Experience.objects.get(id=exp_id)
	experience.delete()
	return redirect('/')

def removeAchievement(request, achv_id):
	achievement = Achievement.objects.get(id=achv_id)
	achievement.delete()
	return redirect('/')